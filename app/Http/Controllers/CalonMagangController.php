<?php

/**
 * @Author: askmhs
 * @Date:   2019-02-06 14:17:42
 * @Last Modified by:   Muhammad Harits Syaifulloh
 * @Last Modified time: 2019-02-08 08:46:54
 */
namespace App\Http\Controllers;

use App\Quota;
use App\CalonMagang;
use Illuminate\Http\Request;
use \Carbon\Carbon as Carbon;
use \Carbon\CarbonPeriod as CarbonPeriod;

class CalonMagangController extends Controller
{

	protected $range;
	protected $quota;
	protected $total_registered;

	/**
	 * Registration processes
	 */
	public function register (Request $request)
	{
		$this->getMonthRange();
		$this->total_registered = CalonMagang::where(\DB::raw('MONTH(end_date)'), '>=', reset($this->range))
			->where(\DB::raw('MONTH(end_date)'), '<=', end($this->range))
			->where("status", "APPROVED")
			->select('id', \DB::raw('DATE_FORMAT(start_date, "%Y-%m") as start_date'), \DB::raw('DATE_FORMAT(end_date, "%Y-%m") as end_date'))
			->orderBy('start_date')
			->get();
			
		$this->quota = Quota::whereIn(\DB::raw('MONTH(date)'), $this->range)->select('id', \DB::raw('DATE_FORMAT(date, "%Y-%m") as date'), 'quota')->orderBy('date')->get();
		
		/**
		 * You can replace the line below using the next logic for the internship candidate
		 * If the chosen date range has 0 remains, must throw error to notify the user
		 * that in the following month is not available for intenship
		 */
		return response()->json($this->getResult());
	}

	/**
	 * Get month range from user submitted data
	 */
	function getMonthRange()
	{
		$period = CarbonPeriod::create(request()->get('start_date'), '1 month', request()->get('end_date'));
		foreach ($period as $dt) {
		    $result[] = $dt->format("m");
		}

		$this->range = $result;
	}

	/**
	 * Check if quota is in range between start_date and end_date
	 */
	function inRange($date, $start_date, $end_date) {
		return ($date->month >= $start_date->month && $date->month <= $end_date->month);
	}

	/**
	 * Calcuate remains quota each month
	 * Each object would have remains attribute
	 * that indicate how many quotas availabe in the month
	 */
	function getResult ()
	{
		$this->quota->transform(function ($q) {
			$q->remains = $q->quota;
			return $q;
		});

		foreach ($this->total_registered as $registered) {
			$start_date = Carbon::parse($registered->start_date)->firstOfMonth();
			$end_date = Carbon::parse($registered->end_date)->lastOfMonth();
			foreach ($this->quota as $quota) {
				$date = Carbon::parse($quota->date)->firstOfMonth();
				if ($this->inRange($date, $start_date, $end_date)) {
					$quota->remains--;
				}				
			}
		}

		return $this->quota;
	}
}