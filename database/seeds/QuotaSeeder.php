<?php

use Illuminate\Database\Seeder;
use App\Quota;

class QuotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
        		'time' => '2019-01-01',
        		'quota' => rand(1,20)
        	],
        	[
        		'time' => '2019-02-01',
        		'quota' => rand(1,20)
        	],
        	[
        		'time' => '2019-03-01',
        		'quota' => rand(1,20)
        	],
        	[
        		'time' => '2019-04-01',
        		'quota' => rand(1,20)
        	],
        	[
        		'time' => '2019-05-01',
        		'quota' => rand(1,20)
        	]
        ];

        foreach ($data as $item) {
        	Quota::create($item);
        }
    }
}
