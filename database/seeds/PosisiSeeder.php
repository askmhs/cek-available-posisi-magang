<?php

use Illuminate\Database\Seeder;
use App\Posisi;

class PosisiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posisi = ['BACKEND ENGINEER', 'FRONTEND ENGINEER', 'QA ENGINEER', 'ANDROID ENGINEER', 'iOS ENGINEER'];
        foreach ($posisi as $item) {
        	Posisi::create([
        		'name' => $item
        	]);
        }
    }
}
